#!/usr/bin/env node

process.env.BABEL_ENV = 'production';
process.env.NODE_ENV = 'production';

require('dotenv').config();

const chalk = require('chalk');
const path = require('path');
const fs = require('fs-extra');
const webpack = require('webpack');
const configFactory = require('./webpack.config');

const appDirectory = fs.realpathSync(process.cwd());

process.env.NODE_PATH = (process.env.NODE_PATH || '')
  .split(path.delimiter)
  .filter((folder) => folder && !path.isAbsolute(folder))
  .map((folder) => path.resolve(appDirectory, folder))
  .join(path.delimiter);

try {
  const config = configFactory('production');

  console.log('Creating an optimized production build...');

  const compiler = webpack(config);
  compiler.run((err, stats) => {
    let messages;
    if (err) {
      if (!err.message) {
        throw err;
      }
      messages = formatWebpackMessages({
        errors: [err.message],
        warnings: [],
      });
    } else {
      messages = formatWebpackMessages(stats.toJson({ all: false, warnings: true, errors: true }));
    }
    if (messages.errors.length) {
      if (messages.errors.length > 1) {
        messages.errors.length = 1;
      }
      throw new Error(messages.errors.join('\n\n'));
    }

    if (!messages.warnings.length) {
      console.log(chalk.green('Compiled successfully.\n'));
    }
  });
} catch (e) {
  if (e && e.message) {
    console.log(e.message);
  }

  process.exit(1);
}

const friendlySyntaxErrorLabel = 'Syntax error:';

function isLikelyASyntaxError(message) {
  return message.indexOf(friendlySyntaxErrorLabel) !== -1;
}

function formatMessage(message) {
  let lines = message.split('\n');

  lines = lines.filter((line) => !/Module [A-z ]+\(from/.test(line));

  lines = lines.map((line) => {
    const parsingError = /Line (\d+):(?:(\d+):)?\s*Parsing error: (.+)$/.exec(line);
    if (!parsingError) {
      return line;
    }
    const [, errorLine, errorColumn, errorMessage] = parsingError;
    return `${friendlySyntaxErrorLabel} ${errorMessage} (${errorLine}:${errorColumn})`;
  });

  message = lines.join('\n');

  message = message.replace(
    /SyntaxError\s+\((\d+):(\d+)\)\s*(.+?)\n/g,
    `${friendlySyntaxErrorLabel} $3 ($1:$2)\n`,
  );
  message = message.replace(
    /^.*export '(.+?)' was not found in '(.+?)'.*$/gm,
    'Attempted import error: \'$1\' is not exported from \'$2\'.',
  );
  message = message.replace(
    /^.*export 'default' \(imported as '(.+?)'\) was not found in '(.+?)'.*$/gm,
    'Attempted import error: \'$2\' does not contain a default export (imported as \'$1\').',
  );
  message = message.replace(
    /^.*export '(.+?)' \(imported as '(.+?)'\) was not found in '(.+?)'.*$/gm,
    'Attempted import error: \'$1\' is not exported from \'$3\' (imported as \'$2\').',
  );
  lines = message.split('\n');

  if (lines.length > 2 && lines[1].trim() === '') {
    lines.splice(1, 1);
  }
  lines[0] = lines[0].replace(/^(.*) \d+:\d+-\d+$/, '$1');

  if (lines[1] && lines[1].indexOf('Module not found: ') === 0) {
    lines = [
      lines[0],
      lines[1]
        .replace('Error: ', '')
        .replace('Module not found: Cannot find file:', 'Cannot find file:'),
    ];
  }

  lines[0] = chalk.inverse(lines[0]);

  message = lines.join('\n');
  message = message.replace(/^\s*at\s((?!webpack:).)*:\d+:\d+[\s)]*(\n|$)/gm, '');
  message = message.replace(/^\s*at\s<anonymous>(\n|$)/gm, '');
  lines = message.split('\n');

  lines = lines.filter(
    (line, index, arr) => index === 0 || line.trim() !== '' || line.trim() !== arr[index - 1].trim(),
  );

  message = lines.join('\n');
  return message.trim();
}

function formatWebpackMessages(json) {
  const formattedErrors = json.errors.map((message) => formatMessage(message, true));
  const formattedWarnings = json.warnings.map((message) => formatMessage(message, false));
  const result = { errors: formattedErrors, warnings: formattedWarnings };
  if (result.errors.some(isLikelyASyntaxError)) {
    result.errors = result.errors.filter(isLikelyASyntaxError);
  }
  return result;
}
