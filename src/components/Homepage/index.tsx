import React from 'react';
import { getServiceProps } from 'service-props';
import FeaturedTechnologies from './components/FeaturedTechnologies';
import Header from './components/Header';

const Homepage = () => {
  const { pageManagement } = getServiceProps();
  pageManagement.setPageTitle('Homepage');

  return (
    <>
      <Header />
      <FeaturedTechnologies />
    </>
  );
};

export default Homepage;
