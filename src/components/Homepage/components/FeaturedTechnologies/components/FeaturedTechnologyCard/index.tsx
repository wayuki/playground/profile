import { Card } from 'antd';
import React from 'react';
import PropTypes from 'prop-types';
import style from './index.module.scss';

interface IFeaturedTechnologyCardProps {
  cover: string
  title: string
  description: React.ReactNode
}

const FeaturedTechnologyCard: React.FC<IFeaturedTechnologyCardProps> = ({
  cover, title, description,
}) => (
  <Card
    cover={(
      <div
        className={style.image}
        style={{ backgroundImage: `url("${cover}")` }}
      >
        <img src={cover} alt={title} />
      </div>
    )}
    style={{
      height: '100%',
    }}
  >
    <Card.Meta
      title={title}
      description={description}
      style={{ textAlign: 'center' }}
    />
  </Card>
);

FeaturedTechnologyCard.propTypes = {
  cover: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.node.isRequired,
};

export default FeaturedTechnologyCard;
