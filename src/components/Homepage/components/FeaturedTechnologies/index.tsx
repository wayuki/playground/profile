import { Col, Row, Typography } from 'antd';
import React from 'react';
import Link from 'components/common/Link';
import Container from 'components/common/Container';
import FeaturedTechnologyCard from './components/FeaturedTechnologyCard';
import style from './index.module.scss';
import singleSpaImg from './images/single-spa.jpg';
import reactJsImg from './images/reactjs.png';
import antDesignImg from './images/ant-design.jpg';
import awsImg from './images/aws.jpg';

const FeaturedTechnologies = () => (
  <Container>
    <div className={style['featured-technologies']}>
      <Typography.Title level={2}>
        Featured Technologies and Tools
      </Typography.Title>
      <Row
        gutter={[{ xs: 0, sm: 16 }, { xs: 16, sm: 16 }]}
        justify="space-between"
        style={{ width: '100%' }}
      >
        <Col lg={{ span: 6 }} sm={{ span: 12 }} xs={{ span: 24 }}>
          <FeaturedTechnologyCard
            cover={singleSpaImg}
            title="Single SPA"
            description={(
              <>
                <Link to="singleSpa">
                  <Typography.Text className={style.link}>Single SPA</Typography.Text>
                </Link>
                {' '}
                manages lifecycles of mounting and un-mounting modules.
                With an in-house module loader, all modules can be mostly independent
                from each other
              </>
            )}
          />
        </Col>
        <Col lg={{ span: 6 }} sm={{ span: 12 }} xs={{ span: 24 }}>
          <FeaturedTechnologyCard
            cover={reactJsImg}
            title="ReactJS"
            description={(
              <>
                <Link to="reactJs">
                  <Typography.Text className={style.link}>ReactJS</Typography.Text>
                </Link>
                {' '}
                is used to build the user interfaces in the micro-services
              </>
            )}
          />
        </Col>
        <Col lg={{ span: 6 }} sm={{ span: 12 }} xs={{ span: 24 }}>
          <FeaturedTechnologyCard
            cover={antDesignImg}
            title="Ant Design"
            description={(
              <>
                The visual of many of the pages are powered by
                {' '}
                <Link to="antDesign">
                  <Typography.Text className={style.link}>Ant Design</Typography.Text>
                </Link>
                {' '}
                framework
              </>
            )}
          />
        </Col>
        <Col lg={{ span: 6 }} sm={{ span: 12 }} xs={{ span: 24 }}>
          <FeaturedTechnologyCard
            cover={awsImg}
            title="Amazon Web Services"
            description={(
              <>
                All static assets are hosted on
                {' '}
                <Link to="awsS3">
                  <Typography.Text className={style.link}>AWS S3</Typography.Text>
                </Link>
                {' '}
                and
                served via
                {' '}
                <Link to="awsCF">
                  <Typography.Text className={style.link}>AWS CloudFront</Typography.Text>
                </Link>
                , which are provisioned
                using
                {' '}
                <Link to="awsCFN">
                  <Typography.Text className={style.link}>AWS CloudFormation</Typography.Text>
                </Link>
              </>
            )}
          />
        </Col>
      </Row>
    </div>
  </Container>
);

export default FeaturedTechnologies;
