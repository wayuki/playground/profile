import { Typography } from 'antd';
import React from 'react';
import Container from 'components/common/Container';
import style from './index.module.scss';

const Header = () => (
  <div className={style['header-container']}>
    <Container>
      <div className={style.header}>
        <Typography.Title>Welcome to Playground</Typography.Title>
        <Typography.Text>Powered by a front-end micro-service architecture</Typography.Text>
      </div>
    </Container>
  </div>
);

export default Header;
