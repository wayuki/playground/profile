import { Divider, Typography } from 'antd';
import React from 'react';
import Project from './components/Project';

const FeaturedProjects = () => (
  <>
    <Typography.Title level={3} style={{ textAlign: 'center' }}>
      Featured Projects
    </Typography.Title>
    <Project
      title="Automated Testing Bots"
      codename="Project Umbrella"
      description={(
        <>
          <Typography.Paragraph>
            To keep the users satisfied, many developers conduct testing on the web applications
            they develop. However, due to the nature of the test automation, the usefulness of
            a test relies heavily on how the test is scripted/implemented. Moreover, it requires
            developers to be familiar with writing tests, which requires very different mindset
            and logic. Furthermore, writing tests to cover every single possible case may not
            always be practically possible, especially from the business point of view.
          </Typography.Paragraph>
          <Typography.Paragraph>
            This project aimed to address the issues aforementioned by developing bots whose actions
            are based on two questions:
            <ol>
              <li>What are the interactable elements on the active page?</li>
              <li>Which of those interactable elements should I interact with?</li>
            </ol>
            and that they:
            <ul>
              <li>require minimal maintenance from the developers;</li>
              <li>are able to cover as many cases as possible, ideally including edge cases;</li>
              <li>can run tests against any context.</li>
            </ul>
          </Typography.Paragraph>
          <Typography.Paragraph>
            The deliverable was a service which generates bots which detect interactable elements
            in the DOM tree given their definition, and pick and interact one of the interactable
            elements in a stochastic way.
          </Typography.Paragraph>
          <Typography.Paragraph>
            After testing it against the newly established web application which is based on
            a front-end micro-service architecture, while it has some drawbacks such as not being
            able to capture logic errors, it was evaluated to be effective in capturing
            system-breaking bugs that had existed and hidden for a long time.
          </Typography.Paragraph>
        </>
      )}
    />
    <Divider />
    <Project
      title="Front-End Micro-Services"
      codename="Project Portal"
      description={(
        <>
          <Typography.Paragraph>
            Many technologies and techniques such as Single Page Application have been developed
            over the past decades to allow web application to be more performant
            while improving the freedom and efficiency of developers.
            However, as the web application grows as well as the number of developers contributing
            to the monolithic codebase, problems arise.
            <ol>
              <li>
                It becomes more and more challenging for each developer to understand
                the entire application, highly increasing the risk of introducing a change
                which accidentally breaks some parts of the application.
              </li>
              <li>
                Every developer is forced to use the same technologies and tools, which may not be
                optimal for every problem to be solved.
              </li>
            </ol>
          </Typography.Paragraph>
          <Typography.Paragraph>
            This project aimed to address the issues aforementioned by developing a new architecture
            to:
            <ol>
              <li>
                significantly reduce the number of scenarios where a change to the codebase breaks
                other parts of the application;
              </li>
              <li>
                empower each team of developers to use technologies and tools they see fit.
              </li>
            </ol>
          </Typography.Paragraph>
          <Typography.Paragraph>
            The deliverable was a new application utilizing an architecture which:
            <ol>
              <li>
                allows parts of the application to be as independent from the others as possible,
                such that each part can reside in its own repository and have their own set of
                dependencies mostly independent from the others;
              </li>
              <li>
                supports data communication among all different parts such as broadcasting messages
                about a state change from one part to the others for those which are interested in
                can react accordingly.
              </li>
            </ol>
          </Typography.Paragraph>
        </>
      )}
    />
    <Divider />
    <Project
      title="Latent Topics-Based Interactive Search Engine"
      codename="Project AcceSE"
      description={(
        <>
          <Typography.Paragraph>
            Despite the advancement of search engines over the years, people with
            intellectual disabilities have found many search engines to be difficult to use
            as those search engines are often designed in a way that assumes users possess the same
            level of cognitive abilities and motor skills, causing that many people with
            intellectual disabilities find it difficult to use for information access. Some of such
            design elements are:
            <ol>
              <li>
                the search results are presented in a list which the ordering depends on the
                predicted relevance and requires users to read the title and description to
                determine which results are likely to be relevant to them;
              </li>
              <li>
                most search engines finish the searching process once the search results are
                retrieved, which quality is heavily dependent on the quality of the submitted query.
              </li>
            </ol>
          </Typography.Paragraph>
          <Typography.Paragraph>
            This project aimed to propose a latent topics-based interactive search
            model, which is an interactive search model utilizing the latent topics distribution of
            the assessed documents and candidate documents to determine the relevancy in addition to
            the query.
          </Typography.Paragraph>
          <Typography.Paragraph>
            The deliverable was a search engine web application that integrates the proposed search
            model. In detailed, only the highest ranked search result is presented to the user. if
            the user determines that the search result is irrelevant after viewing and examining
            the content, the user simply clicks the &#34;Next&#34; button. The system will re-rank
            the search results by updating the parameters of a modified version of the Dirichlet
            distribution based on the result&#39;s probability distribution over the latent topics,
            and present the highest ranked search result.
          </Typography.Paragraph>
          <Typography.Paragraph>
            The search engine was presented to a few candidates with intellectual disabilities, and
            the feedback was positive, proving that the proposed search model can be effective in
            helping people with intellectual disabilities use the search engine by taking the
            explicit relevance feedback into account when retrieving a new search result and
            reducing the amount of information shown on the search result pages.
          </Typography.Paragraph>
        </>
      )}
    />
  </>
);

export default FeaturedProjects;
