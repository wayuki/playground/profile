import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';
import style from './index.module.scss';

interface IProjectProps {
  title: string
  codename: string
  description: React.ReactNode | React.ReactNodeArray
}

const Project: React.FC<IProjectProps> = ({ title, codename, description }) => (
  <div className={style.project}>
    <Typography.Title level={4} className={style.title}>{title}</Typography.Title>
    <Typography.Text className={style.codename}>{codename}</Typography.Text>
    <Typography.Paragraph className={style.description}>{description}</Typography.Paragraph>
  </div>
);

Project.propTypes = {
  title: PropTypes.string.isRequired,
  codename: PropTypes.string.isRequired,
  description: PropTypes.node.isRequired,
};

export default Project;
