import React from 'react';
import { Timeline as AntdTimeline, Typography } from 'antd';
import { BookOutlined, LaptopOutlined } from '@ant-design/icons';
import style from './index.module.scss';

const Timeline = () => (
  <div className={style['timeline-container']}>
    <Typography.Title level={3} style={{ marginTop: '1.2em' }}>Timeline</Typography.Title>
    <AntdTimeline mode="left">
      <AntdTimeline.Item
        dot={<LaptopOutlined />}
        label={(
          <>
            <Typography.Text>January 2018 - Current</Typography.Text>
            <Typography.Title level={5} className={style['position-title']}>
              Software Engineer II
            </Typography.Title>
            <Typography.Text>intelliHR Systems Pty Ltd., Australia</Typography.Text>
          </>
        )}
      >
        <Typography.Paragraph>
          <ul>
            <li>Update, modified, and expanded the core product.</li>
            <li>
              Research, develop, and integrate web technologies, machine learning techniques,
              and natural language processing techniques to the core product
              such as front-end micro-services, topic modelling, and automated testing bots.
            </li>
            <li>Mentor and guide junior engineers in technical aspects.</li>
          </ul>
        </Typography.Paragraph>
      </AntdTimeline.Item>
      <AntdTimeline.Item
        dot={<BookOutlined />}
        label={(
          <>
            <Typography.Text>February 2016 - December 2017</Typography.Text>
            <Typography.Title level={5} className={style['position-title']}>
              Master of Information Technology
            </Typography.Title>
            <Typography.Text>Queensland University of Technology, Australia</Typography.Text>
          </>
        )}
      >
        <Typography.Paragraph>
          <ul>
            <li>Final GPA: 6.875/7.0.</li>
            <li>
              Recipient of QUT Science and Engineering International Merit Scholarship and
              Merit Plus Scholarship.
            </li>
            <li>
              Admitted to Dean&#39;s List of Students with Excellent Academic Performance
              in every semester.
            </li>
            <li>Granted membership of Golden Key International Honour Society.</li>
          </ul>
        </Typography.Paragraph>
      </AntdTimeline.Item>
      <AntdTimeline.Item
        dot={<LaptopOutlined />}
        label={(
          <>
            <Typography.Text>September 2015 - February 2016</Typography.Text>
            <Typography.Title level={5} className={style['position-title']}>
              Web Developer
            </Typography.Title>
            <Typography.Text>iFoundries | Digital Marketing Agency, Singapore</Typography.Text>
          </>
        )}
      >
        <Typography.Paragraph>
          <ul>
            <li>
              Developed mobile friendly websites and portals for multiple clients using Joomla CMS.
            </li>
            <li>
              Participated in pre-project analysis and technical assessments to develop
              user-friendly interface and correct functionality to meet business objectives.
            </li>
            <li>
              Delivered end-user training allowing staff to make easy updates and maintenance to
              website without developer assistance.
            </li>
          </ul>
        </Typography.Paragraph>
      </AntdTimeline.Item>
      <AntdTimeline.Item
        dot={<BookOutlined />}
        label={(
          <>
            <Typography.Text>September 2010 - August 2014</Typography.Text>
            <Typography.Title level={5} className={style['position-title']}>
              Bachelor of Electronic Engineering
            </Typography.Title>
            <Typography.Text>
              Chien Hsin University of Science and Technology, Taiwan
            </Typography.Text>
          </>
        )}
      >
        <Typography.Paragraph>
          <ul>
            <li>
              Final result: 89.32/100.
            </li>
            <li>
              Recipient of International Student Merit Scholarship.
            </li>
            <li>
              Won 5
              <sup>th</sup>
              {' '}
              place in Taiwan Artificial Intelligence Micro-Mouse
              National and International Invitational.
            </li>
          </ul>
        </Typography.Paragraph>
      </AntdTimeline.Item>
      <AntdTimeline.Item />
    </AntdTimeline>
  </div>
);

export default Timeline;
