import {
  Col, Row, Tree, Typography,
} from 'antd';
import React from 'react';
import style from './index.module.scss';

const Highlights = () => (
  <>
    <Typography.Title level={3}>
      Highlights
    </Typography.Title>
    <Row gutter={16} style={{ width: '100%' }}>
      <Col xs={{ span: 24 }} sm={{ span: 12 }}>
        <div className={style['left-tree-container']}>
          <Tree
            defaultExpandAll
            treeData={[
              {
                title: 'Front-End Development',
                key: 'front-end-development',
                children: [
                  {
                    title: 'ECMAScript 6+',
                    key: 'ecmascript',
                  },
                  {
                    title: 'TypeScript',
                    key: 'typescript',
                  },
                  {
                    title: 'Webpack',
                    key: 'webpack',
                  },
                  {
                    title: 'ReactJS Ecosystem',
                    key: 'reactjs',
                  },
                  {
                    title: 'Front-End Micro-Services',
                    key: 'front-end-micro-services',
                  },
                ],
              },
              {
                title: 'DevOps',
                key: 'devops',
                children: [
                  {
                    title: 'Docker',
                    key: 'docker',
                  },
                  {
                    title: 'Kubernetes',
                    key: 'kubernetes',
                  },
                  {
                    title: 'Amazon Web Services',
                    key: 'aws',
                  },
                  {
                    title: 'Jenkins',
                    key: 'jenkins',
                  },
                  {
                    title: 'GitLab CI',
                    key: 'gitlab-ci',
                  },
                ],
              },
            ]}
          />
        </div>
      </Col>
      <Col xs={{ span: 24 }} sm={{ span: 12 }}>
        <Tree
          defaultExpandAll
          treeData={[
            {
              title: 'Back-End Development',
              key: 'back-end-development',
              children: [
                {
                  title: 'Laravel',
                  key: 'laravel',
                },
                {
                  title: 'Hug',
                  key: 'hug',
                },
                {
                  title: 'Celery',
                  key: 'celery',
                },
                {
                  title: 'ExpressJS',
                  key: 'expressjs',
                },
                {
                  title: 'FeathersJS',
                  key: 'feathersjs',
                },
                {
                  title: 'PostgreSQL',
                  key: 'postgresql',
                },
                {
                  title: 'ElasticSearch',
                  key: 'elasticsearch',
                },
              ],
            },
            {
              title: 'Artificial Intelligence',
              key: 'ai',
              children: [
                {
                  title: 'Natural Language Processing',
                  key: 'natural-language-processing',
                },
                {
                  title: 'Topic Modelling',
                  key: 'topic-modelling',
                },
                {
                  title: 'Machine Learning Techniques',
                  key: 'machine-learning-techniques',
                },
              ],
            },
          ]}
        />
      </Col>
    </Row>
  </>
);

export default Highlights;
