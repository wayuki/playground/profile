import {
  Col, Row, Typography,
} from 'antd';
import React from 'react';
import Link from 'components/common/Link';
import {
  GithubOutlined, GitlabOutlined, LinkedinOutlined, MailOutlined,
} from '@ant-design/icons';
import style from './index.module.scss';

const Intro = () => (
  <>
    <Typography.Title level={2}>
      <span className={style['name-breaker']}>Chuen Hong</span>
      {' '}
      <span className={style['name-breaker']}>(Alfred), SEE</span>
    </Typography.Title>
    <Row gutter={[16, 8]} justify="space-between" style={{ width: '80%' }}>
      <Col xs={{ span: 12 }} sm={{ span: 6 }} style={{ textAlign: 'center' }}>
        <Link to="github">
          <Typography.Text className={style.link}>
            <GithubOutlined />
            {' '}
            GitHub
          </Typography.Text>
        </Link>
      </Col>
      <Col xs={{ span: 12 }} sm={{ span: 6 }} style={{ textAlign: 'center' }}>
        <Link to="gitlab">
          <Typography.Text className={style.link}>
            <GitlabOutlined />
            {' '}
            GitLab
          </Typography.Text>
        </Link>
      </Col>
      <Col xs={{ span: 12 }} sm={{ span: 6 }} style={{ textAlign: 'center' }}>
        <Link to="linkedin">
          <Typography.Text className={style.link}>
            <LinkedinOutlined />
            {' '}
            LinkedIn
          </Typography.Text>
        </Link>
      </Col>
      <Col xs={{ span: 12 }} sm={{ span: 6 }} style={{ textAlign: 'center' }}>
        <Link href="mailto:adchuenhong@gmail.com">
          <Typography.Text className={style.link}>
            <MailOutlined />
            {' '}
            Email
          </Typography.Text>
        </Link>
      </Col>
    </Row>
    <Typography.Paragraph className={style.autobiography}>
      I am a full-stack software engineer with experience in
      working in a Singapore digital agency company and intelliHR Systems.
      I have been a proactive learner and am thrilled to apply the knowledge
      and skills I have gained in various area. As I have worked with clients
      from Singapore, Malaysia, Taiwan, and Australia, I have experience in working
      with people from different cultures and different backgrounds. I am always looking
      for an opportunity to confront myself with challenging problems, learn
      different skills and technologies, and contribute to the next great products.
    </Typography.Paragraph>
  </>
);

export default Intro;
