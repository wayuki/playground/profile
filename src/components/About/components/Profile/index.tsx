import React from 'react';
import style from './index.module.scss';
import Highlights from './components/Highlights';
import Intro from './components/Intro';

const Profile = () => (
  <div className={style.profile}>
    <Intro />
    <Highlights />
  </div>
);

export default Profile;
