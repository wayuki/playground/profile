import Container from 'components/common/Container';
import React from 'react';
import { getServiceProps } from 'service-props';
import style from './index.module.scss';
import profilePic from './images/patamon.png';
import Profile from './components/Profile';
import Timeline from './components/Timeline';
import FeaturedProjects from './components/FeaturedProjects';

const About = () => {
  const { pageManagement } = getServiceProps();
  pageManagement.setPageTitle('About');

  return (
    <div className={style['author-container']}>
      <div className={style.author}>
        <div
          className={style['profile-picture']}
          style={{ backgroundImage: `url("${profilePic}")` }}
        >
          <img src={profilePic} alt="Profile Pic" />
        </div>
        <div className={style['profile-container']}>
          <Container>
            <Profile />
            <Timeline />
            <FeaturedProjects />
          </Container>
        </div>
      </div>
    </div>
  );
};

export default About;
