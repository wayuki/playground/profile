import React, { PureComponent } from 'react';
import { Route, Router, Switch } from 'react-router-dom';
import { getServiceProps } from 'service-props';
import * as uuid from 'uuid';
import logger from 'logger';
import Homepage from 'components/Homepage';
import { urlForInternalRoute } from 'components/common/Link';
import About from 'components/About';

interface IAppState {
  error: Error | null
}

class App extends PureComponent<{}, IAppState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      error: null,
    };
  }

  public static getDerivedStateFromError(error: Error) {
    return {
      error,
    };
  }

  public componentDidCatch() {}

  public render() {
    const { singleSpa, router: { history } } = getServiceProps();

    const { error } = this.state;
    if (error) {
      const id = uuid.v4();
      logger.error(error, { id });

      if (window.location.pathname !== '/500') {
        singleSpa.navigateToUrl(`/500?error-id=${id}`);
      }
    }

    return (
      <Router history={history}>
        <Switch>
          <Route
            exact
            path={urlForInternalRoute('home')}
            component={Homepage}
          />
          <Route
            exact
            path={urlForInternalRoute('about')}
            component={About}
          />
        </Switch>
      </Router>
    );
  }
}

export default App;
